-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: imotel
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1


-- -----------------------------------------------------
-- Schema imotel
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `imotel` ;

-- -----------------------------------------------------
-- Schema imotel
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `imotel` DEFAULT CHARACTER SET utf8 ;
USE `imotel` ;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


--
-- Table structure for table `administradores`
--

DROP TABLE IF EXISTS `administradores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administradores` (
  `idAdministrador` int(11) NOT NULL,
  PRIMARY KEY (`idAdministrador`),
  CONSTRAINT `fk_administrador_1` FOREIGN KEY (`idAdministrador`) REFERENCES `usuarios` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administradores`
--

LOCK TABLES `administradores` WRITE;
/*!40000 ALTER TABLE `administradores` DISABLE KEYS */;
/*!40000 ALTER TABLE `administradores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `idCliente` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `email` varchar(60) NOT NULL,
  `telefone` varchar(15) NOT NULL,
  `dataNascimento` date NOT NULL,
  `idEndereco` int(11) NOT NULL,
  PRIMARY KEY (`idCliente`),
  KEY `fk_cliente_endereco_idx` (`idEndereco`),
  CONSTRAINT `fk_cliente_endereco` FOREIGN KEY (`idEndereco`) REFERENCES `enderecos` (`idEndereco`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_clientes_usuario` FOREIGN KEY (`idCliente`) REFERENCES `usuarios` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (1,'Kesse Jones','kessejones@gmail.com','2899966-0241','1997-04-04',1);
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enderecos`
--

DROP TABLE IF EXISTS `enderecos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enderecos` (
  `idEndereco` int(11) NOT NULL AUTO_INCREMENT,
  `cep` varchar(10) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `cidade` varchar(50) NOT NULL,
  `bairro` varchar(50) NOT NULL,
  `rua` varchar(100) NOT NULL,
  PRIMARY KEY (`idEndereco`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enderecos`
--

LOCK TABLES `enderecos` WRITE;
/*!40000 ALTER TABLE `enderecos` DISABLE KEYS */;
INSERT INTO `enderecos` VALUES (1,'29360000','ES','Castelo','Centro','12');
/*!40000 ALTER TABLE `enderecos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fotosQuartos`
--

DROP TABLE IF EXISTS `fotosQuartos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fotosQuartos` (
  `idFotoQuarto` int(11) NOT NULL AUTO_INCREMENT,
  `idQuarto` int(11) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `destaque` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idFotoQuarto`),
  KEY `fk_fotosQuartos_quartos_idx` (`idQuarto`),
  CONSTRAINT `fk_fotosQuartos_quartos` FOREIGN KEY (`idQuarto`) REFERENCES `quartos` (`idQuarto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fotosQuartos`
--

LOCK TABLES `fotosQuartos` WRITE;
/*!40000 ALTER TABLE `fotosQuartos` DISABLE KEYS */;
INSERT INTO `fotosQuartos` VALUES (1,1,'classe_a_1.jpeg',0),(2,1,'classe_a_2.jpeg',1),(3,2,'classe_a_1.jpeg',0),(4,2,'classe_a_2.jpeg',1),(5,3,'classe_a_1.jpeg',0),(6,3,'classe_a_2.jpeg',1),(7,4,'luxo_1.jpg',0),(8,4,'luxo_2.jpg',0),(9,4,'luxo_3.jpg',1),(10,5,'luxo_1.jpg',0),(11,5,'luxo_2.jpg',0),(12,5,'luxo_3.jpg',1),(13,6,'luxo_1.jpg',0),(14,6,'luxo_2.jpg',0),(15,6,'luxo_3.jpg',1),(16,7,'presidencial_1.jpg',1),(17,8,'presidencial_1.jpg',1),(18,9,'presidencial_1.jpg',1),(19,10,'simples_1.jpg',1),(20,10,'simples_2.jpg',0),(21,10,'simples_3.jpg',0),(22,10,'simples_4.jpg',0),(23,10,'simples_5.jpg',0),(24,10,'simples_6.jpg',0),(25,10,'simples_7.jpg',0),(26,10,'simples_8.jpg',0),(27,10,'simples_9.jpg',0),(28,11,'simples_1.jpg',1),(29,11,'simples_2.jpg',0),(30,11,'simples_3.jpg',0),(31,11,'simples_4.jpg',0),(32,11,'simples_5.jpg',0),(33,11,'simples_6.jpg',0),(34,11,'simples_7.jpg',0),(35,11,'simples_8.jpg',0),(36,11,'simples_9.jpg',0),(37,12,'simples_1.jpg',1),(38,12,'simples_2.jpg',0),(39,12,'simples_3.jpg',0),(40,12,'simples_4.jpg',0),(41,12,'simples_5.jpg',0),(42,12,'simples_6.jpg',0),(43,12,'simples_7.jpg',0),(44,12,'simples_8.jpg',0),(45,12,'simples_9.jpg',0);
/*!40000 ALTER TABLE `fotosQuartos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quartos`
--

DROP TABLE IF EXISTS `quartos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quartos` (
  `idQuarto` int(11) NOT NULL AUTO_INCREMENT,
  `idTipoQuarto` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  PRIMARY KEY (`idQuarto`),
  KEY `fk_quartos_tipoQuarto_idx` (`idTipoQuarto`),
  CONSTRAINT `fk_quartos_tipoQuarto` FOREIGN KEY (`idTipoQuarto`) REFERENCES `tiposQuarto` (`idTipoQuarto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quartos`
--

LOCK TABLES `quartos` WRITE;
/*!40000 ALTER TABLE `quartos` DISABLE KEYS */;
INSERT INTO `quartos` VALUES (1,1,1),(2,1,2),(3,1,3),(4,2,1),(5,2,2),(6,2,3),(7,3,1),(8,3,2),(9,3,3),(10,4,1),(11,4,2),(12,4,3);
/*!40000 ALTER TABLE `quartos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quartosClientes`
--

DROP TABLE IF EXISTS `quartosClientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quartosClientes` (
  `idQuartoCliente` int(11) NOT NULL AUTO_INCREMENT,
  `idCliente` int(11) NOT NULL,
  `idQuarto` int(11) NOT NULL,
  `dataReserva` date NOT NULL,
  `dataSaida` date DEFAULT NULL,
  `total` float DEFAULT NULL,
  PRIMARY KEY (`idQuartoCliente`),
  KEY `fk_quartoCliente_Cliente_idx` (`idCliente`),
  KEY `fk_quartoCliente_Quarto_idx` (`idQuarto`),
  CONSTRAINT `fk_quartoCliente_Cliente` FOREIGN KEY (`idCliente`) REFERENCES `clientes` (`idCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_quartoCliente_Quarto` FOREIGN KEY (`idQuarto`) REFERENCES `quartos` (`idQuarto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quartosClientes`
--

LOCK TABLES `quartosClientes` WRITE;
/*!40000 ALTER TABLE `quartosClientes` DISABLE KEYS */;
INSERT INTO `quartosClientes` VALUES (1,1,1,'2017-12-02','2017-12-04',340),(2,1,1,'2017-12-10','2017-12-15',850),(3,1,1,'2017-12-20','2017-12-25',850);
/*!40000 ALTER TABLE `quartosClientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiposQuarto`
--

DROP TABLE IF EXISTS `tiposQuarto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiposQuarto` (
  `idTipoQuarto` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `descricao` varchar(1000) NOT NULL,
  `preco` float NOT NULL,
  PRIMARY KEY (`idTipoQuarto`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiposQuarto`
--

LOCK TABLES `tiposQuarto` WRITE;
/*!40000 ALTER TABLE `tiposQuarto` DISABLE KEYS */;
INSERT INTO `tiposQuarto` VALUES (1,'Classe A','Possui tudo o que as outras suítes têm, decoração moderna, porém num só andar, TV de LCD, com canais por assinatura, 3 canais de vídeo erótico, som AM/FM, Ar Condicionado, Banheiro com ducha e Garagem privativa.\nAlgumas contam com frigobar',170),(2,'Luxo','Uma confortável duplex de aproximadamente 300m² toda decorada na cor vermelha.\n\nNa parte inferior a suite tem mesinha para refeição, sofás, TV, piscina com cascata e deck de madeira, sauna à vapor, banheiro, um belíssimo jardim de inverno, lareira, ar condicionado, espelhos grandes nas paredes.\n\nNa parte superior um agradável espaço com cama e colchão de molas, banheira de hidro, poltrona erótica, teto solar, TV de LCD 42\", canais por assinatura, 3 canais de vídeo erótico, som AM/FM\n\n• Piscina com aquecedor e cascata.\n• As suítes também possuem espelho sobre a cama.\n• Garagem dupla.\n• Acabamentos em gesso, granito e porce-lanato.',230),(3,'Presidencial','Possuem dois ambientes, TV LCD, Banheira de hidro massagem, ar condicionado,algumas com frigobar, colchão de molas, jardim de inverno, área externa com mesinha e cadeiras.\n\nAs suítes também possuem alguns itens como espelho sobre a cama, garagem privativa.\n\nAcabamentos em gesso, paredes texturizadas, granito e porcelanato.\n\nNa Suíte Duplex, temos mais uma opção do motel, banheira japonesa - o ofurô com jatos de hidromassagem.',300),(4,'Selvagem','aquecedor - Ar quente e frio - cadeira erótica - cama king size - CD player - cine privê - decoração temática - DVD - espelho no teto - frigobar - garagem privativa - hidro - jardim de inverno - pista de dança - pole dance - secador de cabelo - som ambiente - TV a cabo - TV LCD 32\" - Wi-Fi',500);
/*!40000 ALTER TABLE `tiposQuarto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL,
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'kessejones','$2y$10$iIACDbQBxeM5k01Sm7zdz.pqAwiAErGoZA8KsjG5/LjGi48KoM/Pm');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-28 13:38:53
