/*
*
* Função que verifica o cep e preenche Endereço
* 
*/

jQuery(function($){
   $("#cep").change(function(){
      var cep_code = $(this).val();
      if( cep_code.length <= 0 ) return;
      $.get("http://apps.widenet.com.br/busca-cep/api/cep.json", { code: cep_code },
         function(result){
            if( result.status!=1 ){
               
               swal({
                  title: 'Houve um erro desconhecido',
                  text: 'Por favor, Tente novamente.',
                  timer: 2000
                  }).then(
                  function () {},
                  // handling the promise rejection
                  function (dismiss) {
                     if (dismiss === 'timer') {
                       
                  }
               })
               return;
            }
            $("input#cep").val( result.code );
            $("input#estado").val( result.state );
            $("input#cidade").val( result.city );
            $("input#bairro").val( result.district );
            $("input#rua").val( result.address );

            $("input#estado").attr('readonly', true);
            $("input#cidade").attr('readonly', true);
         });
   });
});

/*
*
* Função para bloquear caracteres
*
*/

function BloquearCaracteres(evnt){
   if (evnt.keyCode < 48 || evnt.keyCode > 57)
   {
      swal({
         title: 'Caracter inválido',
         text: 'Por favor, Tente novamente usando apenas números.',
         timer: 2000
         }).then(
         function () {},
         // handling the promise rejection
         function (dismiss) {
            if (dismiss === 'timer') {
              
         }
      })
      return false
   }
   else
   {
      if ((evnt.charCode < 48 || evnt.charCode > 57) && evnt.keyCode == 0){
         return false
      }
   }
}

/*
*
* Função para bloquear numeros
*
*/
function BloquearNumeros(evnt){
   if (evnt.keyCode < 48 || evnt.keyCode > 57)
   {
      return true
   }
   else
   {
      if ((!evnt.charCode < 48 || !evnt.charCode > 57) && !evnt.keyCode == 0){
            swal({
            title: 'Caracter inválido',
            text: 'Por favor, Tente novamente usando apenas letras.',
            timer: 2000
            }).then(
            function () {},
            // handling the promise rejection
            function (dismiss) {
               if (dismiss === 'timer') {
                 
            }
         })
         return false
      }
   }
}

/*
*
* Função para Verificar e-mail
*
*/

function VerificarEmail(field) {
   usuario = field.value.substring(0, field.value.indexOf("@"));
   dominio = field.value.substring(field.value.indexOf("@")+ 1, field.value.length);

   if ((usuario.length >=1) &&
      (dominio.length >=3) && 
      (usuario.search("@")==-1) && 
      (dominio.search("@")==-1) &&
      (usuario.search(" ")==-1) && 
      (dominio.search(" ")==-1) &&
      (dominio.search(".")!=-1) &&      
      (dominio.indexOf(".") >=1)&& 
      (dominio.lastIndexOf(".") < dominio.length - 1)) {
      
      return true;
   }
   else
   {
      swal({
         title: 'E-mail inválido',
         text: 'Por favor, Tente novamente com um e-mail válido.',
         timer: 2000
         }).then(
         function () {},
         // handling the promise rejection
         function (dismiss) {
            if (dismiss === 'timer') {
              
         }
      })

      var email = document.getElementById("email");
      email.focus();
   }
}