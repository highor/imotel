var input = document.getElementById('imagem');
var preview = document.querySelector('.preview');


input.addEventListener('change', updateImageDisplay);

function updateImageDisplay() {
  while(preview.firstChild) {
    preview.removeChild(preview.firstChild);
  }

  var curFiles = input.files;

  if(curFiles.length === 0) {
    var para = document.createElement('p');
    para.textContent = 'Nenhum arquivo selecionado para upload.';
    preview.appendChild(para);
  } else {
    var list = document.createElement('ol');
    preview.appendChild(list);
    for(var i = 0; i < curFiles.length; i++) {
      var listItem = document.createElement('li');
      listItem.setAttribute('onclick', 'setDestaque(this);');
      listItem.setAttribute('class', 'imgNaoSelecionada');
      listItem.setAttribute("name", "" + curFiles[i].name);
      var nome = document.createElement('p');
      if(validFileType(curFiles[i])) {
        nome.textContent = 'Nome do arquivo: ' + curFiles[i].name;
        var tam = document.createElement('p');
        tam.textContent = 'Tamanho: ' + returnFileSize(curFiles[i].size);
        var image = document.createElement('img');
        image.src = window.URL.createObjectURL(curFiles[i]);

        listItem.appendChild(image);
        listItem.appendChild(nome);
        listItem.appendChild(tam);

      } else {
        nome.textContent = 'O arquivo ' + curFiles[i].name + ' Não corresponde a um tipo válido. Por favor, escolha outro arquivo.';
        listItem.appendChild(nome);
      }

      list.appendChild(listItem);
    }
  }
}

var fileTypes = [
  'image/jpg',
  'image/jpeg',
  'image/png',
  'image/bmp'
]

function validFileType(file) {
  for(var i = 0; i < fileTypes.length; i++) {
    if(file.type === fileTypes[i]) {
      return true;
    }
  }

  return false;
}

function returnFileSize(number) {
  if(number < 1024) {
    return number + 'bytes';
  } else if(number > 1024 && number < 1048576) {
    return (number/1024).toFixed(1) + 'KB';
  } else if(number > 1048576) {
    return (number/1048576).toFixed(1) + 'MB';
  }
}


function setDestaque(imagem)
{
    
    var itens = document.getElementsByClassName('imgNaoSelecionada');
        
    for (var i = 0; i < itens.length; i++) {
            
        itens[i].style.background = "";
    }
    
    imagem.style.background = "rgba(0, 50, 25, 0.8)";
    
    imgDestaque = document.getElementById('foto_destaque');
    imgDestaque.value = imagem.getAttribute("name");

    destaqueAtual = document.getElementsByClassName('foto_destaque');

    for (var i = 0; i < destaqueAtual.length; i++)
    {
      destaqueAtual[i].setAttribute("onclick", false);
    }    
}

