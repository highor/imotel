$( "#cadastrar_quarto" ).submit(function( event ) {
  	if (document.getElementById("foto_destaque").value == "") {
		swal({
		    title: 'Quarto não cadastrado',
		    text: 'Por favor, clique em uma imagem para ser a imagem de destaque do quarto.',
		    timer: 2000
		    }).then(
		    function () {},
		    // handling the promise rejection
		    function (dismiss) {
			    if (dismiss === 'timer') {
			        
			    }
		    }
		)
		return false;
	}
	return true;
	 event.preventDefault();
});