<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Quarto;
use App\TipoQuarto;


class HomeController extends Controller
{
    /**
	* Retorna a view especifica de acordo com o tipo de usuário.
	* @return Response
	*/

    public function index()
    {
        if(Auth::check())
        {
        	$usuario = Auth::user();
            
            if($usuario->isAdmin())
            {
                return view('administrador.index');
            }
            else
            {
                return view('cliente.index');
            }
        }
        else
        {
        
            $tipos_quartos = TipoQuarto::all();
            $quartos = Quarto::all();

            return view('index', ['tipos_quartos' => $tipos_quartos, 'quartos' => $quartos]);
        }	
    }
}
