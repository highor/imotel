<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Log;
use App\Quarto;
use App\TipoQuarto;
use App\FotoQuarto;

class QuartoController extends Controller
{
    public function listar()
    {
    	$tipos_quartos = TipoQuarto::all();
        $quartos = Quarto::all();
        return view('quartos.listar', ['tipos_quartos' => $tipos_quartos, 'quartos' => $quartos]);
    }

    public function buscarTipo($TipoQuarto)
    {
        $tipos_quartos = TipoQuarto::where('idTipoQuarto', '=', $TipoQuarto)->get();
        $quartos = Quarto::where('idTipoQuarto', '=', $TipoQuarto)->get();
        return view('quartos.listar', ['tipos_quartos' => $tipos_quartos, 'quartos' => $quartos]);
    }

    public function obterQuarto($idquarto)
    {
        $quarto = Quarto::find($idquarto);
        $fotos = FotoQuarto::where('idQuarto', '=', $idquarto)->get();
        return view('quartos.obter_quarto', ['quarto' => $quarto, 'fotos' => $fotos]);
    }

    public function cadastrar()
    {
        $tipos_quartos = TipoQuarto::all();
        $quartos = Quarto::all();
        return view('quartos.cadastrar', ['tipos_quartos' => $tipos_quartos]);
    }

    public function inserir(Request $request)
    {

        $dados = $request->except(['_token']);

        DB::beginTransaction();

        try
        {
            $quarto = Quarto::create(['idTipoQuarto' => $dados['tipo_quarto'], 'numero' => $dados['numero']]);

            $fotos = $request->file('imagens');

            for ($i = 0; $i < count($fotos); $i++) {
                
                $fotos[$i]->move(storage_path() . '/imagens', $fotos[$i]->getClientOriginalName());

                if ($fotos[$i]->getClientOriginalName() == $dados['foto_destaque']) {
                    FotoQuarto::create(['idQuarto' => $quarto->idQuarto, 'foto'=>$fotos[$i]->getClientOriginalName(), 'destaque' => '1']);
                }
                else
                    FotoQuarto::create(['idQuarto' => $quarto->idQuarto, 'foto'=>$fotos[$i]->getClientOriginalName()]);
            }

            DB::commit();
        }
        catch(\Exception $e)
        {
            DB::rollback();
            Log::info('ERRO', ['E' => $e]);
            return redirect('/')->with(['msg' => 'erro']);
        }

        return redirect('/')->with(['msg' => 'ok']);
    }
}
