<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use DB;
use Validator;
use App\User;
use App\Cliente;
use App\Endereco;

class ClienteController extends Controller
{
    public function index()
    {
    	return view('cliente.index');
    }

    public function listar()
    {
    	$clientes = Cliente::all();
    	return view('cliente.listar', ['clientes'=>$clientes]);
    }

    public function cadastrar()
    {
    	return view('cliente.cadastrar');
    }

    public function inserir()
    {
    	$request = \Request::all();
        extract($request);

        DB::beginTransaction();

        try
        {
            $usuario = User::create(['login' => $login, 'password' => bcrypt($senha)]);
            
            $endereco = Endereco::create(['cep' => $cep, 'estado' => $estado, 'cidade' => $cidade, 'bairro' => $bairro, 'rua' => $rua]); 

            Cliente::create(['idCliente' => $usuario->idUsuario, 'nome' => $nome, 'email' => $email, 'telefone' => $telefone, 'dataNascimento' => $dataNascimento,'idEndereco'=> $endereco->idEndereco]);

            DB::commit();
        }
        catch(\Exception $e)
        {
            DB::rollback();
            Log::info('ERRO', ['E' => $e]);
            return redirect('/')->with(['msg' => 'erro']);
        }

        return redirect('/')->with(['msg' => 'ok']);
    }
}
