<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\TipoQuarto;
use App\FotosQuarto;

class TipoQuartoController extends Controller
{
    public function listar()
    {
    	$tipos_quartos = TipoQuarto::all();

        return view('tiposquartos.listar', ['tipos_quartos' => $tipos_quartos]);
    }

    // public function buscarTipo($TipoQuarto)
    // {
    //     $tipo_quarto = TipoQuarto::where('idTipoQuarto', '=', $tipos_quartos)->get();
    //     return view('tiposquartos.listar', ['tipo_quarto' => $tipo_quarto]);
    // }

    public function cadastrar()
    {
        return view('tiposquartos.cadastrar');
    }

    public function inserir(Request $request)
    {
        $dados = $request->except(['_token']);
        

        DB::beginTransaction();

        try
        {
            TipoQuarto::create(['nome'=>$dados['nome'], 'descricao'=>$dados['descricao'], 'preco'=>$dados['preco']]);
            DB::commit();
        }
        catch(Exception $e)
        {
            DB::rollback();
            Log::info('ERRO', ['E' => $e]);
            return redirect('/cadastrar/tiposquartos')->with(['msg' => 'erro']);
        }

        return redirect('/')->with(['msg' => 'ok']);
    }
}
