<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Image;
use File;

class FotoQuartoController extends Controller
{
    public function fotoQuarto($nomeImagem, $largura, $altura)
	{
		$foto = Image::make(storage_path() . '/imagens/' . $nomeImagem)->resize($largura, $altura);


		return $foto->response(File::extension(storage_path() . '/imagens/' . $nomeImagem));
	}
}
