<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use DB;
use Exception;
use Auth;
use App\TipoQuarto;
use App\Quarto;
use App\Reserva;

class ReservaController extends Controller
{
    public function listar()
    {
        $tipo_quarto = TipoQuarto::all();
        $cliente = Auth::user()->cliente->idCliente;
    	$reservas = Reserva::where('idCliente', '=', $cliente)->get();
        return view('reserva.index', ['reservas' => $reservas, 'tipos_quartos'=>$tipo_quarto]);
    }

    public function cadastrar($idquarto)
    {
    	$quarto = Quarto::find($idquarto);
        return view('reserva.cadastrar', ['quarto' => $quarto]);
    }

    public function atualizar($idReserva)
    {
        $reserva = Reserva::find($idReserva);
        return view('reserva.atualizar', ['reserva' => $reserva]);
    }

    public function inserir(Request $request, $idQuarto)
    {
    	$cliente = Auth::user()->cliente->idCliente;
    	$dados = $request->except('_token');
    	$quarto = Quarto::find($idQuarto);
    	$dados['idCliente'] = $cliente;
    	$dados['idQuarto'] = $idQuarto;
        
        if (count(Reserva::where('dataReserva', '>=', $dados['dataReserva'],'and','dataSaida', '<=', $dados['dataSaida'], 'and', 'dataSaida', '<=', $dados['dataSaida'],'and','dataReserva', '>=', $dados['dataReserva'])->where('idQuarto', '=', $idQuarto)->get()) > 0){
            
            return redirect('/')->with(['msg' => 'data']);
        }
        
        DB::beginTransaction();

        try
        {

            $preco_suite = $quarto->tipoQuarto->preco;
        
            $dReserva = date('z', strtotime($dados['dataReserva'])) +1;
            $dSaida = date('z', strtotime($dados['dataSaida'])) +1;
            
            $dias = $dSaida - $dReserva;

            if ($dias == 0) {
                $dados['total'] = $preco_suite;
            }
            else{
               $dados['total'] = $dias * $preco_suite;
            }

            Reserva::create($dados);

            DB::commit();    
    	}
    	catch(Exception $e)
    	{
    		DB::rollback();
            Log::info('ERRO', ['E' => $e]);
            return redirect('/')->with(['msg' => 'erro']);
        }

        return redirect('/')->with(['msg' => 'ok']);
    }

    public function salvarAtualizacao(Request $request, $idReserva)
    {
        $dados = $request->except('_token');
        $reserva = Reserva::find($idReserva);
        $idQuarto = Quarto::find($reserva->quarto->idQuarto);

        if (count(Reserva::where('dataReserva', '>=', $dados['dataReserva'],'and','dataSaida', '<=', $dados['dataSaida'], 'and', 'dataSaida', '<=', $dados['dataSaida'],'and','dataReserva', '>=', $dados['dataReserva'])->where('idQuarto', '=', $idQuarto)->get()) > 0){
            
            return redirect('/')->with(['msg' => 'data']);
        }  

        DB::beginTransaction();
        
        try
        {

            $preco_suite = $reserva->quarto->tipoQuarto->preco;
        
            $dReserva = date('z', strtotime($dados['dataReserva'])) +1;
            $dSaida = date('z', strtotime($dados['dataSaida'])) +1;
            
            $dias = $dSaida - $dReserva;

            if ($dias == 0) {
                $dados['total'] = $preco_suite;
            }
            else{
               $dados['total'] = $dias * $preco_suite;
            }

            $reserva->update($dados);

            DB::commit();    
        
            
        }
        catch(Exception $e)
        {
            DB::rollback();
            Log::info('ERRO', ['E' => $e]);
            return redirect('/')->with(['msg' => 'erro']);
        }

        return redirect('/')->with(['msg' => 'ok']);
    }

    public function excluir($idReserva)
    {
        $reserva = Reserva::find($idReserva);

        DB::beginTransaction();
        
        try
        {

            $reserva->delete();
            DB::commit();
        }
        catch(Exception $e)
        {
            DB::rollback();
            \Log::info('Erro ao excluir a reserva', ['Exception' => $e]);
            return redirect('/')->with(['msg' => 'erro']);
        }
        return redirect('/')->with(['msg' => 'ok']);
    }
}
