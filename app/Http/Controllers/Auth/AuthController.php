<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'sair']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
    * Obtém a autenticação do usuário
    * @var array
    */
    public function getLogin()
    {
        return redirect('/');
    }

    /**
    * Faz a autenticação do usuário
    * @return Response
    */
    public function postLogin()
    {
        $dados = ['login' => \Request::input('login'), 'senha' => \Request::input('senha')];

        $validador = Validator::make($dados, [
            'login' => 'required|min:4|max:60',
            'senha' => 'required|min:6'
        ]);

        $data_auth = ['login' => $dados['login'], 'password' => $dados['senha']];

        if($validador->fails())
        {
            return redirect('/login')->withErrors($validador);
        }
        
        if(Auth::attempt($data_auth))
        {
            return redirect('/');
        }
        // return var_dump($data_auth);
        return redirect('/home');
    }

    /**
    * Logout do sistema
    * @return Response
    */
    public function sair()
    {
        Auth::logout();
        return redirect('/login');
    }
}
