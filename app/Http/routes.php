<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

use App\User;
use App\Administrador;

Route::group(['middleware' => ['web']], function () {
    
	Route::get('/inserir_usuario', function(){
		$user = User::create(['login' => 'Highor', 'password' => bcrypt('highor123')]);
		Administrador::create(['idAdministrador' => $user->idUsuario]);
	});

	Route::get('/', 'HomeController@index');
	Route::get('/home', 'HomeController@index');
    Route::auth();
    Route::get('/login', 'Auth\AuthController@getLogin');
	Route::post('/login', 'Auth\AuthController@postLogin');
    Route::get('/sair', 'Auth\AuthController@sair');
	
	/**
	*
	*Rotas do administrador
	*
	*/
    Route::get('/administrador', 'AdministradorController@index');

    /**
    *
    *Rotas do Cliente
    *
    */
    Route::get('/cliente', 'ClienteController@index');
    Route::get('/cadastrar/cliente', 'ClienteController@cadastrar');
    Route::post('/cadastrar/cliente', 'ClienteController@inserir');

    /**
    *
    *Rotas do tipo de quarto
    *
    */
    Route::get('cadastrar/tipoquarto', 'TipoQuartoController@cadastrar');
    Route::post('cadastrar/tipoquarto', 'TipoQuartoController@inserir');
    Route::get('listar/tiposquartos', 'TipoQuartoController@listar');
    // Route::get('/listar/tipoquarto/{idTipoQuarto}', 'TipoQuartoController@buscarTipo');

    /**
    *
    *Rotas de quarto
    *
    */
    Route::get('cadastrar/quarto', 'QuartoController@cadastrar');
    Route::post('cadastrar/quarto', 'QuartoController@inserir');
    Route::get('listar/quartos', 'QuartoController@listar');
    Route::get('listar/quartos/{tipo_quarto}', 'QuartoController@buscarTipo');
    Route::get('listar/quarto/{quarto}', 'QuartoController@obterQuarto');

    /**
    *
    *Rotas de fotos dos quarto
    *
    */
    Route::get('/foto/{nomeImagem}/{largura}/{altura}', 'FotoQuartoController@fotoQuarto');

    /**
    *
    *Rotas de reserva
    *
    */
    Route::get('reservar/quarto/{quarto}', 'ReservaController@cadastrar');
    Route::post('cadastrar/reserva/{quarto}', 'ReservaController@inserir');
    Route::get('listar/reserva', 'ReservaController@listar');
    Route::get('excluir/reserva/{reserva}', 'ReservaController@excluir');
    Route::get('atualizar/reserva/{reserva}', 'ReservaController@atualizar');
    Route::post('atualizar/reserva/{reserva}', 'ReservaController@salvarAtualizacao');
});
