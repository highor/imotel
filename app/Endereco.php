<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    protected $table = 'enderecos';
    protected $primaryKey = 'idEndereco';

    protected $fillable = ['cep', 'estado', 'cidade', 'bairro', 'rua'];

    public $timestamps = false;

    public function cliente()
    {
    	return $this->hasOne('App\Cliente', 'idCliente', 'idEndereco');
    }
}
