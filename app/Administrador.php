<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Administrador extends Model
{
   	protected $table = 'administradores';
    protected $primaryKey = 'idAdministrador';
    protected $fillable = ['idAdministrador'];
    
    public $timestamps = false;


    public function usuario()
    {
    	return $this->hasOne('App\User', 'idUsuario', 'idAdministrador');
    }
}
