<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'clientes';
    protected $primaryKey = 'idCliente';

    protected $fillable = ['nome', 'email', 'telefone', 'dataNascimento', 'idEndereco', 'idCliente'];

    public $timestamps = false;

    public function usuario()
    {
    	return $this->hasOne('App\User', 'idUsuario', 'idCliente');
    }

    public function endereco()
    {
    	return $this->hasOne('App\Endereco', 'idEndereco', 'idEndereco');
    }

}
