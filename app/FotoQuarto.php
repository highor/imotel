<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FotoQuarto extends Model
{
    protected $table = 'fotosQuartos';
    protected $primaryKey = 'idFotoQuarto';

    protected $fillable = ['idQuarto', 'foto', 'destaque'];

    public $timestamps = false;

    public function quarto()
    {
    	return $this->hasOne('App\Quarto', 'idQuarto', 'idQuarto');
    }
}
