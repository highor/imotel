<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoQuarto extends Model
{
    protected $table = 'tiposQuarto';
    protected $primaryKey = 'idTipoQuarto';
    
    protected $fillable = ['nome', 'descricao', 'preco'];

    public $timestamps = false;

    public function quarto()
    {
    	return $this->hasMany('App\Quarto', 'idQuarto', 'idTipoQuarto');
    }

}
