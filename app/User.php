<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    protected $table = 'usuarios';
    protected $primaryKey = 'idUsuario';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public $timestamps = false;

    /**
     * Retorna o objeto do administrador caso o usuário seja administrador
     * @return Administrador
     */
    public function administrador(){
        return $this->hasOne('App\Administrador', 'idAdministrador', 'idUsuario');
    }

    /**
     * Verifica se o usuário é administrador
     * @return boolean
     */
    public function isAdmin()
    {
        return count($this->administrador) == 1 ? true : false;
    }

    /**
     * Retorna o objeto do tipo cliente caso o usuário seja cliente
     * @return Administrador
     */
    public function cliente()
    {
        return $this->hasOne('App\Cliente', 'idCliente', 'idUsuario');
    }

    /**
     * Verifica se o usuário é cliente
     * @return boolean
     */
    public function isCliente()
    {
        return count($this->cliente) == 1 ? true : false;
    }

    public function getRememberToken()
    {
        return null; // not supported
    }

    public function setRememberToken($value)
    {
        // not supported
    }

    public function getRememberTokenName()
    {
        return null; // not supported
    }
}
