<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    protected $table = 'quartosClientes';
    protected $primaryKey = 'idQuartoCliente';

    protected $fillable = ['idCliente', 'idQuarto', 'dataReserva', 'dataSaida', 'total'];

    public $timestamps = false;

    public function quarto()
    {
    	return $this->hasOne('App\Quarto', 'idQuarto', 'idQuarto');
    }

    public function cliente()
    {
    	return $this->hasOne('App\Cliente', 'idCliente', 'idCliente');
    }
}
