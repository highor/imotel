<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quarto extends Model
{
    protected $table = 'quartos';
    protected $primaryKey = 'idQuarto';

    protected $fillable = ['numero', 'idTipoQuarto'];

    public $timestamps = false;

    public function tipoQuarto()
    {
    	return $this->hasOne('App\TipoQuarto', 'idTipoQuarto', 'idTipoQuarto');
    }

    public function foto()
    {
    	return $this->hasMany('App\FotoQuarto', 'idQuarto', 'idQuarto');
    }

    public function fotoQuarto()
    {
        $fotos = $this->foto->filter(function($img){
            return $img->destaque == 1;
        });
        return $fotos;
    }
}
