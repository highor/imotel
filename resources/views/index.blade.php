@extends ('layouts.layout_default')
@section ('index')
<div class="painel_quem_somos" align="center">
        <img src="{{asset('img/fachada.png')}}" class="fachada">
        <img src="{{asset('img/jardim.png')}}" class="jardim">
        <div class="quem_somos">
            <p>
                O Divinus Motel tem como compromisso proporcionar aos seus clientes uma estadia realmente agradável e prazerosa. 

                Todas as suítes foram projetadas para proporcionar um clima exótico a diferentes níveis de exigências e gostos.

                Nós do Divinus Motel temos a preocupação em manter o padrão de qualidade em cada uma das suítes. 

                Desde a suíte mais simples até a mais sofisticada, todas recebem os mesmos cuidados, seja na escolha dos produtos utilizados ou nos detalhes da decoração.

                Além de bom gosto e conforto, todas as suítes oferecem uma decoração moderna. Conheça nossas <a href="{{ url('/listar/tiposquartos') }}">suites</a>. 
            </p>
        </div>
</div>
    
    <div class="localizacao" align="center">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3674.324253337315!2d-45.377449585033794!3d-22.938282484995863!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cce5df901f6555%3A0x3ab09ea78f59708a!2sDivinus+Motel!5e0!3m2!1spt-BR!2sbr!4v1505474652431" frameborder="0" allowfullscreen></iframe>
    </div>
    @if(session('msg') == 'ok')
        <div id="modal" class="modal">
            <div class="conteudo_modal">
                <label>Sucesso!</label>
            </div>
        </div>
    @elseif(session('msg') == 'erro')
        <div id="modal" class="modal">
            <div class="conteudo_modal">
                <label>Algo deu errado!</label>
            </div>
        </div>
    @elseif(count($errors))
        <div id="modal" class="modal">
            <div class="conteudo_modal">
            @foreach($errors->all() as $erro)
                <label>{{$erro}}</label><br>
            @endforeach
            </div>
        </div>
    @endif
    <script src="{{ asset('scripts/script_modal.js') }}"></script>
@endsection
