@extends ('layouts.layout_cliente')
@section ('painel')
	
	<div class="conteudo_quartos">
		<div class="cabecalho_conteudo_quartos">
			<h3>Lista de Tipos de Quartos</h3>
		</div>
		@if(Auth::check())
			@if(Auth::user()->isAdmin())
				<a class="botao_cadastrar" href="{{ url('/cadastrar/tipoquarto') }}">Cadastrar</a>				
		    @endif
		@endif
		@foreach($tipos_quartos as $tipo_quarto)
	    	<div class="quarto">
				<div class="conteudo_quartos">
					<div class="cabecalho_conteudo_quartos">
		    			<h2>Suite {{ $tipo_quarto->nome }}</h2>
		    		</div>
		    		<div class="descricao_geral">	
			    		<p>
			    			{{ $tipo_quarto->descricao }}
			    		</p>
			    		<br>
						<h3>Valor da Diaria R$ {{ number_format($tipo_quarto->preco, 2, ',', '') }}</h3>
						@if(Auth::check())
							@if(Auth::user()->isCliente())
								<br><a class="botao_paineis" href="{{ url('/listar/quartos', $tipo_quarto->idTipoQuarto) }}">Listar Suites</a>
							@endif
						@endif

						@if(!Auth::check())
								<br><a href="{{ url('/cadastrar/cliente') }}">cadastre-se</a> e reserve uma suite.
						@endif
					</div>
				</div>	
			</div>		
		@endforeach
	</div>
@endsection