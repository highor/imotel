@extends ('layouts.layout_administrador')
@section ('titulo', 'Cadastro de Tipo de Quarto')
@section ('painel')

	<div id="cadastrar_tipoQuarto" class="formCadastro" align="center">
        <form name="cadastrar_tipo_quarto" method="POST" action="{{ url('/cadastrar/tipoquarto') }}">
            {{ csrf_field() }}
            <div class="elemento">
            	<input type="text" name="nome" data-validate="required" placeholder="Nome" autofocus></input>
            </div>
            <div class="elemento">
            	<textarea name="descricao" data-validate="required" placeholder="Descricao"></textarea> 
            </div>
            <div class="elemento">
            	<input type="text" name="preco" data-validate="required" placeholder="Preço" onkeypress="return BloquearCaracteres(event);"></input>
            </div>
            <div class="elemento">
            	<button type="submit">Cadastrar</button>
            </div>
        </form>
    </div>	
@endsection