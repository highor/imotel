@extends ('layouts.layout_administrador')
@section ('painel')
    <div class="painel_bemvindo">
        <label>Bem Vindo {{ Auth::user()->login }}</label>
    </div>

    <div class="menu">
        <ul>
        	<li>
        		<a class="botao_paineis" href="{{ url('/listar/tiposquartos') }}">Tipos de Quarto</a>
        	</li>

        	<li>
        		<a class="botao_paineis" href="{{ url('/listar/quartos') }}">Quarto</a>
        	</li>
            <li>
                <a class="botao_paineis" href="{{ url('/sair') }}">Sair</a>
            </li>
        </ul>
    </div>
    @if(session('msg') == 'ok')
        <div id="modal" class="modal">
            <div class="conteudo_modal">
                <label>Sucesso!</label>
            </div>
        </div>
    @elseif(session('msg') == 'erro')
        <div id="modal" class="modal">
            <div class="conteudo_modal">
                <label>Algo deu errado!</label>
            </div>
        </div>
    @elseif(count($errors))
        <div id="modal" class="modal">
            <div class="conteudo_modal">
            @foreach($errors->all() as $erro)
                <label>{{$erro}}</label><br>
            @endforeach
            </div>
        </div>
    @endif
    <script src="{{ asset('scripts/script_modal.js') }}"></script>
@endsection