@extends ('layouts.layout_cliente')
@section ('titulo', 'Cadastro de Cliente')
@section ('painel')

    <div id="cadastrar_cliente" class="formCadastro" align="center">
        <form name="cadastrar_cliente" method="POST" action="{{ url('/cadastrar/cliente') }}">
            {{ csrf_field() }}
            <div class="elemento">
            	<input type="text" name="login" data-validate="required" placeholder="Login" autofocus></input>
            </div>
            <div class="elemento">
            	<input type="password" name="senha" data-validate="required" placeholder="Senha"></input>
            </div>
            <div class="elemento">
            	<input type="text" name="nome" data-validate="required" placeholder="Nome" onkeypress="return BloquearNumeros(event);"></input>
            </div>
            <div class="elemento">
                <input type="date" name="dataNascimento" data-validate="required" placeholder="Data de Nascimento" min="{{ date('Y-m-d', strtotime('-100 Years')) }}" max="{{ date('Y-m-d', strtotime('-18 Years')) }}"></input>
            </div>
            <div class="elemento">
            	<input type="text" name="email" id="email" data-validate="required" placeholder="e-mail" onblur="VerificarEmail(cadastrar_cliente.email)"></input>
            </div>
            <div class="elemento">
            	<input type="tel" name="telefone" data-validate="required" placeholder="Telefone" onkeypress="return BloquearCaracteres(event);"></input>
            </div>
            <div class="elemento">
                <input type="text" name="cep" id="cep" data-validate="required" placeholder="Cep" onkeypress="return BloquearCaracteres(event);"></input>
            </div>
            <div class="elemento">
                <input type="text" name="estado" id="estado" data-validate="required" placeholder="Estado" onkeypress="return BloquearNumeros(event);"></input>
            </div>
            <div class="elemento">
                <input type="text" name="cidade" id="cidade" data-validate="required" placeholder="Cidade"></input>
            </div>
            <div class="elemento">
                <input type="text" name="bairro" id="bairro" data-validate="required" placeholder="Bairro"></input>
            </div>
            <div class="elemento">
                <input type="text" name="rua" id="rua" data-validate="required" placeholder="Rua"></input>
            </div>
            <div class="elemento">
            	<button type="submit">Cadastrar</button>
            </div>
        </form>
    </div>
@endsection