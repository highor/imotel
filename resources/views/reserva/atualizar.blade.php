@extends ('layouts.layout_cliente')
@section ('titulo', 'Atualizar Reserva')
@section ('painel')

	<div id="atualizar_reserva" class="formCadastro" align="center">
        <form id="atualizar_reserva" name="atualizar_reserva" method="POST" enctype="multipart/form-data" action="{{ url('/atualizar/reserva', $reserva->idQuartoCliente) }}">
            {{ csrf_field() }}
            
            <div class="elemento">
                <label>Data de Entrada</label>
            </div>
            <div class="elemento">
            	<input type="date" name="dataReserva" data-validate="required" placeholder="Data Entrada" autofocus min="{{ date('Y-m-d') }}" value="{{$reserva->dataReserva}}"></input>
            </div>
            <div class="elemento">
                <label>Data de Saida</label>
            </div>
            <div class="elemento">
                <input type="date" name="dataSaida" data-validate="required" placeholder="Data Saida" autofocus min="{{ date('Y-m-d') }}" value="{{$reserva->dataSaida}}"></input>
            </div>
            <div class="elemento">
            	<button type="submit">Atualizar</button>
            </div>
        </form>
    </div>
    <script type="text/javascript" src="{{ asset('scripts/script_add_imagens.js') }}"></script>
    <script type="text/javascript" src="{{ asset('scripts/script_verificar_imagem.js') }}"></script>
@endsection