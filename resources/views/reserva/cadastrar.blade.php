@extends ('layouts.layout_cliente')
@section ('titulo', 'Cadastro de Reservas')
@section ('painel')

	<div id="cadastrar_reserva" class="formCadastro" align="center">
        <form id="cadastrar_reserva" name="cadastrar_reserva" method="POST" enctype="multipart/form-data" action="{{ url('/cadastrar/reserva', $quarto->idQuarto) }}">
            {{ csrf_field() }}
            
            <div class="elemento">
                <label>Data de Entrada</label>
            </div>
            <div class="elemento">
            	<input type="date" name="dataReserva" data-validate="required" placeholder="Data Entrada" autofocus min="{{ date('Y-m-d') }}"></input>
            </div>
            <div class="elemento">
                <label>Data de Saida</label>
            </div>
            <div class="elemento">
                <input type="date" name="dataSaida" data-validate="required" placeholder="Data Saida" autofocus min="{{ date('Y-m-d') }}"></input>
            </div>
            <div class="elemento">
            	<button type="submit">Cadastrar</button>
            </div>
        </form>
    </div>
    <script type="text/javascript" src="{{ asset('scripts/script_add_imagens.js') }}"></script>
    <script type="text/javascript" src="{{ asset('scripts/script_verificar_imagem.js') }}"></script>
@endsection