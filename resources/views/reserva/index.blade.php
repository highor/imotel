@extends ('layouts.layout_cliente')
@section ('painel')
@foreach($reservas as $reserva)
    <div class="quarto">
            <div class="conteudo_quartos">                 
                        <header class="cabecalho_conteudo_quartos">
                            <h1>Suite {{ $reserva->quarto->tipoQuarto->nome }}-{{ $reserva->quarto->numero }}</h1>
                        </header>
                        <div class="descricao_geral">
                            <p>{{ $reserva->quarto->tipoQuarto->descricao }}</p>
                            <br>
                            <h3>Valor da Diaria: R$ {{ number_format($reserva->quarto->tipoQuarto->preco, 2, ',', '') }}</h3>
                            <br>
                            <h3>Data da Reserva: {{ date('d/m/Y', strtotime($reserva->dataReserva)) }}</h3>
                            <br>
                            <h3>Data de Saida: {{ date('d/m/Y', strtotime($reserva->dataSaida)) }}</h3>
                            <br>
                            <h3>Reservada por: {{ ((date('z', strtotime($reserva->dataSaida)) +1) - (date('z', strtotime($reserva->dataReserva)) +1))}} dias</h3>
                            <br>
                            <h3>Valor Total da Reserva: R$ {{ number_format($reserva->total, 2, ',', '') }}</h3>
                            <br>
                            <a href = "{{ url('/atualizar/reserva', $reserva->idQuartoCliente) }}" class="botao_paineis">Atualizar</a>
                            <a href = "{{ url('/excluir/reserva', $reserva->idQuartoCliente) }}" class="botao_paineis">Cancelar</a>
                    </div>
            </div>
            <br><br>
    </div>
@endforeach
@endsection
<script type="text/javascript" src="{{ asset('scripts/script_slide_quartos.js') }}"></script>