<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <title>DIVINUS MOTEL</title>

    <link rel="stylesheet" type="text/css" href="{{ asset('css/estilo.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/sweetalert2.min.css') }}">

    <script type="text/javascript" src="{{ asset('scripts/sweetalert2.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('scripts/jquery-3.2.1.min.js') }}"></script>

     <script type="text/javascript" src="{{ asset('scripts/script_verifica_cadastro.js') }}"></script>

     <script src="{{ asset('scripts/verify.notify.js') }}"></script>
    
</head>

<body>
    <div class="cabecalho" align="center">
        <a href="/home"><img src="{{ asset('img/logotipo.png')}}"></a>
    </div>  
        
    <div class="painel" align="center">
        @yield ('painel')
    </div>

    <footer>
        <div align="center">
            <label>Divinus Motel - </label>
            <label>Rodovia Presidente Dutra, Km 91 </label>
            <label>- Feital - Pindamonhangaba - SP - (012) 3637-1845</label>
        </div>
    </footer>
</body>

</html>