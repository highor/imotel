@extends ('layouts.layout_cliente')
@section ('painel')
<div class="conteudo_quartos" align="center">
	<div class="cabecalho_conteudo_quartos">
		<h3>Suite {{$quarto->tipoQuarto->nome}}-{{$quarto->numero}}</h3>
	</div>
	<div class="quarto" align="center">
		@foreach($fotos as $foto)
			<div class="imagens_quartos">
				<div class="cabecalho_conteudo_quartos">
					<h5>Imagens do Quarto</h5>
				</div>
				<a href="/foto/{{$foto->foto}}/680/680"><img src="/foto/{{$foto->foto}}/340/240"></a>
			</div>
		@endforeach
		<div class="descricao_quartos">
			<div class="cabecalho_conteudo_quartos">
				<h5>Descrição</h5>
			</div>
			<div class="descricao_geral">
				<p> {{$quarto->tipoQuarto->descricao}}</p>
			</div>
			<div class="descricao_geral">
				<a class="botao_paineis" href="{{ url('/reservar/quarto', $quarto->idQuarto) }}"> Reservar esta suite</a>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="{{ asset('scripts/script_slide_quartos.js') }}"></script>
@endsection