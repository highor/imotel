@extends ('layouts.layout_cliente')
@section ('painel')
		@if(Auth::check())
			@if(Auth::user()->isAdmin())
				<a class="botao_paineis" href="{{ url('/cadastrar/quarto') }}">Cadastrar</a>				
		    @endif
		@endif

		@foreach($tipos_quartos as $tipo_quarto)
		<div class="conteudo_quartos">
			<div class="cabecalho_conteudo_quartos">
				<h2>Suite {{ $tipo_quarto->nome }}</h2>
			</div>
			<div class="quarto">
					<div class="descricao_geral">
						<h3>{{ $tipo_quarto->descricao }}</h3>
						<h3>Valor da Diaria R$ {{ number_format($tipo_quarto->preco, 2, ',', '') }}</h3>
					</div>	
					@foreach($quartos as $quarto)
						@if($tipo_quarto->idTipoQuarto == $quarto->idTipoQuarto)
							<div class="conteudo_quartos">
								<div class="cabecalho_conteudo_quartos">
									<label> Suite {{ $tipo_quarto->nome }}-{{$quarto->numero}}</label><br>
								</div>
								@foreach($quarto->fotoQuarto() as $foto)
		                			<br><a href=""><img src="/foto/{{$foto->foto}}/200/200"></a><br>
			            		@endforeach
			            		@if(Auth::check())
									@if(Auth::user()->isCliente())
										<a class="botao_paineis" href="{{ url('/listar/quarto', $quarto->idQuarto) }}">Ver fotos</a>				
								    @endif
							    @endif	
			            	</div>
			            	<br><br>
						@endif
					@endforeach				
			</div>
		</div>
		<br><br>
		@endforeach
@endsection	