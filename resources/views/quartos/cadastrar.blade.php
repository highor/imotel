@extends ('layouts.layout_administrador')
@section ('titulo', 'Cadastro de Tipo de Quarto')
@section ('painel')

	<div id="cadastrar_Quarto" class="formCadastro" align="center">
        <form id="cadastrar_quarto" name="cadastrar_quarto" method="POST" enctype="multipart/form-data" action="{{ url('/cadastrar/quarto') }}">
            {{ csrf_field() }}
            <div class="elemento">
                <select name="tipo_quarto" data-validate="required">
                    <option value="" selected>selecione um tipo de quarto</option>
                    @foreach($tipos_quartos as $tipo_quarto)
                        <option value="{{ $tipo_quarto->idTipoQuarto }}">{{ $tipo_quarto->nome }}</option>
                    @endforeach
                </select>
            </div>

            <div class="elemento">
            	<input type="text" name="numero" data-validate="required" placeholder="Numero" autofocus onkeypress="return BloquearCaracteres(event);"></input>
            </div>
            
            <div class="elemento">
                <label>Fotos do Quarto</label><br>
                <input type="file" id="imagem" name="imagens[]" data-validate="required" accept=".jpg, .jpeg, .png, .bmp" multiple>
                <input type="hidden" id="foto_destaque" name="foto_destaque" value="">
                <div class="preview">
                    <p>Nenhum arquivo selecionado para upload</p>
                </div>
             </div>

            <div class="elemento">
            	<button type="submit">Cadastrar</button>
            </div>
        </form>
    </div>
    <script type="text/javascript" src="{{ asset('scripts/script_add_imagens.js') }}"></script>
    <script type="text/javascript" src="{{ asset('scripts/script_verificar_imagem.js') }}"></script>
@endsection